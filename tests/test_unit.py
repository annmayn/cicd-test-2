import unittest
import main

class Test(unittest.TestCase):
    def test_add(self):
        self.assertEqual(main.add(2,3),5)

    def test_add_negative(self):
        self.assertEqual(main.add(-2,-5),-7)